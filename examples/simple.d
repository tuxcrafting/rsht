import rsht.application;
import rsht.util;

import std.conv;
import std.stdio;
import std.string;

void main() {
    Application app = new Application();

    app.on(`^.*$`, `^/$`, Method.GET | Method.PUT, (req, res) {
        res.header("Content-Type", "text/plain");
        yieldchunk("Hello");
    });

    app.listen(new InternetAddress("127.0.0.1", 8000));
    app.listen(new InternetAddress("127.0.1.1", 7000), 1);
    app.listen(new InternetAddress("127.0.1.1", 8000), 2);
    app.listen(new InternetAddress("127.0.1.1", 9000), 255);
    app.start;
}
