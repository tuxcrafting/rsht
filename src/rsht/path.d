// rsht/path.d - URI parser

module rsht.path;

import std.regex;
import std.string;
import std.uri;

/// Path structure and parser.
struct Path {
    /// Raw path.
    string raw;
    /// Protocol.
    string protocol;
    /// Domain.
    Captures!string host;
    /// Path.
    Captures!string path;
    /// Query strings.
    string[string] query;
    /// Anchor.
    string anchor;

    private static immutable Regex!char URIRegex = regex(
        `^(([^:/?#]+):)?(//([^/?#]*))?([^?#]*)(\?([^#]*))?(#(.*))?$`
    );
    private static immutable Regex!char QueryParamRegex = regex(
        `^([^#]+?)=([^#]*)$`
    );

    /**
     * Parse URI.
     * Params:
     *     uri    = URI to parse
     *     hostre = Host regex
     *     pathre = Path regex
     **/
    void parse(string uri, Regex!char hostre, Regex!char pathre) {
        Captures!string c = uri.matchFirst(URIRegex);
        if (!c) {
            throw new Exception("Couldn't parse URI");
        }

        raw = uri;

        protocol = c[2];

        host = c[4].decode.matchFirst(hostre);
        if (!host) {
            throw new Exception("Couldn't parse URI");
        }

        path = c[5].decode.matchFirst(pathre);
        if (!path) {
            throw new Exception("Couldn't parse URI");
        }

        anchor = c[9].decode;

        foreach (string q; c[7].split("&")) {
            Captures!string p = q.matchFirst(QueryParamRegex);
            if (!p) {
                throw new Exception("Couldn't parse query parameter");
            }

            query[p[1]] = p[2].decode;
        }
    }

    /**
     * String representation.
     * Params:
     *     justPath = Only return the resource path and query params
     * Returns: String representation of the URI.
     **/
    string toString(bool justPath=false) {
        string s;
        if (justPath) {
            s = path[0];
        } else {
            s = format("%s://%s%s", protocol, host[0], path[0]);
        }
        long i = 0;
        foreach (string key, string value; query) {
            s ~= format("%c%s=%s", i == 0 ? '?' : '&', key, value);
            i++;
        }
        if (!anchor.empty) {
            s ~= format("#%s", anchor);
        }
        return s;
    }
}

unittest {
    Path uri;
    uri.parse(
        "http://hello.world/whats/up?a=b&c=d#anchor",
        regex(`^([^.]+)`),
        regex(`^/([^/]+)`)
    );
    import std.stdio;
    assert(uri.protocol == "http");
    assert(uri.host[1] == "hello");
    assert(uri.path[1] == "whats");
    assert(uri.query["a"] == "b");
    assert(uri.query["c"] == "d");
    assert(uri.anchor == "anchor");

    Path uri1;
    uri1.parse("https://google.com/idk/lol?a=b", regex(`^.*$`), regex(`^.*$`));
    assert(uri1.toString == "https://google.com/idk/lol?a=b");
}
