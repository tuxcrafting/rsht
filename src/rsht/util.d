// rsht/util.d - Utility functions

module rsht.util;

public import std.concurrency;
import std.conv;
import std.datetime;
import std.string;

/**
 * Convert a date to a RFC 1123 compliant string.
 * Params:
 *     date = Date and time
 * Returns: Date string.
 **/
string date1123(SysTime date) {
    SysTime utcDate = date.toUTC;
    return format(
        "%s, %02d %s %04d %02d:%02d:%02d GMT",

        utcDate.dayOfWeek.to!string.capitalize,

        utcDate.day,
        utcDate.month.to!string.capitalize,
        utcDate.year,

        utcDate.hour,
        utcDate.minute,
        utcDate.second
    );
}

unittest {
    import std.stdio;
    assert(date1123(SysTime(
        DateTime(2018, 4, 17, 18, 1, 23),
        cast(immutable(TimeZone))new SimpleTimeZone(dur!"hours"(2))
    )) == "Tue, 17 Apr 2018 16:01:23 GMT");
}

/**
 * Yield with newline.
 * Params:
 *     s = String
 **/
void yieldln(string s) {
    yield(s ~ "\n");
}

/**
 * Formatted yield.
 * Params:
 *     s    = String
 *     args = Arguments
 **/
void yieldf(Args...)(string s, Args args) {
    yield(format(s, args));
}

/**
 * Formatted yield with newline
 * Params:
 *     s    = String
 *     args = Arguments
 **/
void yieldfln(Args...)(string s, Args args) {
    yield(format(s, args) ~ "\n");
}

/**
 * Yield a fixed length string, chunked in 1KB blocks
 * Params:
 *     s      = String
 *     length = Yield length
 **/
void yieldchunk(string s) {
    yield(s.length.to!string);
    long j = 0;
    string buf = "";
    for (long i = 0; i < s.length; i++) {
        buf ~= s[i];
        j++;
        if (j == 1024) {
            yield(buf);
            j = 0;
            buf = "";
        }
    }
    if (j) {
        yield(buf);
    }
}
