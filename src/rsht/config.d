// rsht/config.d - Compile time options

module rsht.config;

/// RSHT version.
enum string VERSION = "1.2.2";

/// Maximum size of the HTTP request header.
enum long MAX_HEADER_SIZE = 4096;

/// Maximum size of the HTTP request entity.
enum long MAX_ENTITY_SIZE = 8388608;

/// Default max-age cache directive.
enum long MAX_AGE_DEFAULT = 86400;
