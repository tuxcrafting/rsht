// rsht/http.d - HTTP request/response struct and parser

module rsht.http;

import rsht.httpstatus;
import rsht.path;

import std.algorithm;
import std.range;
import std.socket;
import std.string;

/// HTTP methods.
enum Method {
    GET = 1,
    POST = 2,
    PUT = 4,
    DELETE = 8,

    HEAD = 16,

    OPTIONS = 32,
    TRACE = 64,
    CONNECT = 128,

    ALL = 15, // GET | POST | PUT | DELETE
}

/**
 * HTTP methods as strings.
 **/
immutable Method[string] MethodNames;

static this() {
    MethodNames = [
        "OPTIONS": Method.OPTIONS,
        "GET": Method.GET,
        "HEAD": Method.HEAD,
        "POST": Method.POST,
        "PUT": Method.PUT,
        "DELETE": Method.DELETE,
        "TRACE": Method.TRACE,
        "CONNECT": Method.CONNECT,
    ];
}

/**
 * HTTP versions.
 **/
enum Version {
    HTTP10,
    HTTP11,
}

/**
 * HTTP versions as strings.
 **/
immutable string[] VersionNames = [
    "1.0",
    "1.1",
];

/**
 * HTTP error class
 **/
class HTTPError : Exception {
    /// Status code
    long status;

    this(long status, string msg) {
        super(msg);
        this.status = status;
    }
}

/**
 * HTTP request message.
 **/
struct RequestMessage {
    /// HTTP method
    Method method;
    /// Path of the resource
    Path path;
    /// HTTP version
    Version httpVersion;
    /// Headers
    string[][string] headers;
    /// Request body
    string requestBody;
    /// Client IP address
    Address address;
    /// Server IP address
    Address destAddress;

    /**
     * Get a comma-separated list of all values of a header.
     * Params:
     *     header = Name of the header
     * Returns: Comma-separated list of all values of a header.
     **/
    string getHeaderValue(string header) {
        string[]* headerPtr = (header in headers);
        if (headerPtr) {
            return (*headerPtr).join(", ");
        } else {
            return null;
        }
    }

    /**
     * Get a list of all values of a header.
     * Params:
     *     header = Name of the header
     * Returns: List of all values of a header.
     **/
    string[] getHeaderValues(string header) {
        string[]* headerPtr = (header in headers);
        if (headerPtr) {
            return *headerPtr;
        } else {
            return null;
        }
    }

    /**
     * Return a string representation of the request header.
     * Returns: String representation of the request header.
     **/
    string toString() {
        string res = format(
            "%s %s HTTP/%s",
            method,
            path.toString(true),
            VersionNames[httpVersion]
        );

        foreach (string k, string[] v; headers) {
            res ~= format("%s: %s\r\n", k, v.join(", "));
        }

        res ~= "\r\n";

        return res;
    }
}

/**
 * HTTP response message.
 **/
struct ResponseMessage {
    /// HTTP version.
    Version httpVersion;
    /// Status code.
    long statusCode;
    /// Headers.
    string[][string] headers;
    /// Max-age (0 for no caching, -1 for infinity).
    long maxAge;

    /**
     * Return a string representation of the response header.
     * Returns: String representation of the response header.
     **/
    string toString() {
        string res = format(
            "HTTP/%s %d %s\r\n",
            VersionNames[httpVersion],
            statusCode,
            StatusCodes[statusCode]
        );

        foreach (string k, string[] v; headers) {
            res ~= format("%s: %s\r\n", k, v.join(", "));
        }

        res ~= "\r\n";

        return res;
    }

    /**
     * Set the value of a header.
     * Params:
     *     header = Header name
     *     value  = Value of the header, may be a comma-separated list
     **/
    void header(string header, string value) {
        headers[header] = value.split(",").map!(x => x.strip).array;
    }

    /**
     * Set the value of a header.
     * Params:
     *     header = Header name
     *     value  = Values of the header
     **/
    void header(string header, string[] value) {
        headers[header] = value.map!(x => x.strip).array;
    }

    /**
     * Get a comma-separated list of all values of a header.
     * Params:
     *     header = Name of the header
     * Returns: Comma-separated list of all values of a header.
     **/
    string getHeaderValue(string header) {
        string[]* headerPtr = (header in headers);
        if (headerPtr) {
            return (*headerPtr).join(", ");
        } else {
            return null;
        }
    }

    /**
     * Get a list of all values of a header.
     * Params:
     *     header = Name of the header
     * Returns: List of all values of a header.
     **/
    string[] getHeaderValues(string header) {
        string[]* headerPtr = (header in headers);
        if (headerPtr) {
            return *headerPtr;
        } else {
            return null;
        }
    }
}
