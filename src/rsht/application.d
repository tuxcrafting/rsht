// vim: fdm=marker
// rsht/application.d - Main application file

module rsht.application;

version (DigitalMars) {
    static assert(
        0,
        "RSHT compiled with DMD is broken, please use a real compiler!"
    );
}

//{{{ Imports
import core.stdc.signal;
import core.stdc.stdlib;

import devnt;

import rsht.config;
public import rsht.http;
import rsht.httpstatus;
public import rsht.path;
import rsht.util;

import std.algorithm;
import std.container;
import std.conv;
import std.datetime;
public import std.experimental.logger;
import std.range;
public import std.regex;
public import std.socket;
import std.stdio;
import std.string;
import std.utf;
//}}}

extern(C) @system @nogc nothrow void handleTermination(int _) {
    DefaultEventLoop.stop;
}

/// Main application class
class Application {
    /// Access logger
    Logger accessLog;
    /// General logger
    Logger generalLog;

    /// Create an application.
    this() {
        accessLog = new FileLogger(stdout);
        generalLog = new FileLogger(stdout);

        signal(SIGINT, &handleTermination);
        signal(SIGTERM, &handleTermination);
    }

    //{{{ Listen
    /**
     * Listen on an address.
     * Params:
     *     addr  = Internet address
     *     proxy = The PROXY protocol version to use on the listener
     *             or 0 to indicate that it's a direct listener
     *             255 indicates X-Forwarded-For is used
     * See_Also: https://www.haproxy.org/download/1.8/doc/proxy-protocol.txt
     **/
    void listen(Address addr, ubyte proxy=0) {
        Socket listener = new Socket(
            addr.addressFamily,
            SocketType.STREAM,
            ProtocolType.TCP
        );
        listener.setOption(
            SocketOptionLevel.SOCKET,
            SocketOption.REUSEADDR,
            1
        );
        listener.bind(addr);
        listener.listen(10);

        // register the listener for read events (connection requests)
        DefaultEventLoop.set(listener.handle, Event.Read, &handleAccept);

        generalLog.infof(
            "[%d] Listening on %s:%s",
            listener.handle,
            addr.toAddrString,
            addr.toPortString
        );

        listeners[listener.handle] = listener;
        isProxy[listener.handle] = proxy;
    }
    //}}}

    ~this() {
        foreach (int _, SocketData data; connections) {
            data.conn.shutdown(SocketShutdown.BOTH);
            data.conn.close;
        }

        foreach (int _, Socket listener; listeners) {
            listener.shutdown(SocketShutdown.BOTH);
            listener.close;
        }
    }

    /// Start the webserver.
    void start() {
        DefaultEventLoop.run(1000);
        generalLog.tracef("Bye!");
    }

    /// Stop the webserver.
    void stop() {
        DefaultEventLoop.stop;
    }

    /// Handle a connection request.
    void handleAccept(IEventLoop loop, int fd, Event event) {
        Socket conn = listeners[fd].accept;

        int cfd = conn.handle;

        conn.blocking = false;

        try {
            connections[cfd] = SocketData(
                this,
                conn,
                listeners[fd]
            );
        } catch (SocketOSException) {
            // prevent crash is socket is closed during init
            generalLog.tracef(
                "[%d] Connection was closed during initialization",
                cfd,
            );
            return;
        }

        generalLog.tracef(
            "[%d] Accepted connection (%s:%s <-> %s)",
            cfd,
            listeners[fd].localAddress.toAddrString,
            listeners[fd].localAddress.toPortString,
            conn.remoteAddress.toAddrString
        );

        // register the connection for read and error events
        DefaultEventLoop.set(cfd, Event.Read, &handleClient);
        DefaultEventLoop.set(cfd, Event.Error, &handleClient);
    }

    /// Handle a client.
    void handleClient(IEventLoop loop, int fd, Event event) {
        if (event == Event.Error) {
            connections.remove(fd);

            DefaultEventLoop.set(fd, Event.Read, null);
            DefaultEventLoop.set(fd, Event.Write, null);
            DefaultEventLoop.set(fd, Event.Error, null);

            generalLog.tracef(
                "[%d] Connection error",
                fd
            );
        } else {
            if (!connections[fd].handleData) {
                connections.remove(fd);

                DefaultEventLoop.set(fd, Event.Read, null);
                DefaultEventLoop.set(fd, Event.Write, null);
                DefaultEventLoop.set(fd, Event.Error, null);

                generalLog.tracef(
                    "[%d] Connection closed by peer",
                    fd
                );
            }
        }
    }

    //{{{ Handlers
    /**
     * Register an error handler.
     * Params:
     *     handler = Request handler
     *               Takes request, response, status code and error message
     **/
    void on(void delegate(
        RequestMessage,
        ResponseMessage*,
        long,
        string
    ) handler) {
        errorhandler = handler;
    }

    /**
     * Register a normal request handler.
     * Params:
     *     host    = Regex of host
     *     path    = Regex of path
     *     method  = HTTP method
     *     handler = Request handler
     *               Takes request, response and path object
     *     hook    = Indicates the route is a "hook": that it does some
     *               processing before passing the request to the actual
     *               handler
     **/
    void on(Regex!char host, Regex!char path, Method method, void delegate(
        RequestMessage,
        ResponseMessage*
    ) handler, bool hook=false) {
        urls.insertBack(URLBinding(
            host,
            path,
            method,
            hook,
            handler
        ));
    }

    /**
     * Register a normal request handler.
     * Params:
     *     host    = Regex of host
     *     path    = Regex of path
     *     method  = HTTP method
     *     handler = Request handler
     *               Takes request, response and path object
     *     hook    = Indicates the route is a "hook": that it does some
     *               processing before passing the request to the actual
     *               handler
     **/
    void on(string host, string path, Method method, void delegate(
        RequestMessage,
        ResponseMessage*
    ) handler, bool hook=false) {
        on(regex(host), regex(path), method, handler, hook);
    }
    //}}}

private:
    //{{{ Structs
    struct URLBinding {
        Regex!char host;
        Regex!char path;
        Method method;
        bool hook;
        void delegate(
            RequestMessage,
            ResponseMessage*
        ) handler;
    }

    struct SocketData {
        //{{{ States
        enum State : long {
            Start,
            Finish,

            Proxy1,

            Proxy2Start,
            Proxy2Header,
            Proxy2Address,

            SendHeaders,
            SendFixedLength,
            SendChunked,

            ParseHTTP1StatusLine,
            ParseHTTP1Header,
            ParseHTTP1Body,

            ParseChunkLength,
            ParseChunkBody,

            ReadUntilCRLF,
            ReadFixedLength,
        }
        //}}}

        static immutable Regex!char StatusLine = regex(
            `^([A-Z]+) (\S+) HTTP/(\d+)\.(\d+)$`
        );
        static immutable Regex!char Header = regex(
            `^([^\x00- \(\)<>@,;:\\"/\[\]\?={}]+):\s*(.*?)\s*$`
        );
        static immutable Regex!char Proxy = regex(
            `^PROXY (\S+) (\S+) (\S+) (\d+) (\d+)$`
        );

        //{{{ Definitions
        Application app;

        Socket conn;
        RequestMessage req;
        ResponseMessage res;
        HTTPError error;
        int fd;
        ubyte proxyLevel;

        State state;
        State nextState0, nextState1;
        bool doJump;
        long r0, r1;
        long reqlen, reslen;
        bool parseHeaders;
        string buf;
        string spath;
        Generator!string resgen;
        //}}}

        this(Application app, Socket conn, Socket listener) {
            this.app = app;
            this.conn = conn;
            fd = conn.handle;
            req.address = conn.remoteAddress;
            req.destAddress = conn.localAddress;
            reset;
            proxyLevel = app.isProxy[listener.handle];
            switch (proxyLevel) {
            case 1:
                doJump = true;
                state = State.ReadUntilCRLF;
                nextState0 = State.Proxy1;
                break;
            case 2:
                state = State.Proxy2Start;
                break;
            default:
                break;
            }
        }

        void reset() {
            req.requestBody = "";
            req.headers.clear;
            error = null;
            state = State.Start;
            reqlen = 0;
            reslen = 0;
            buf = "";
            spath = "";
        }

        void close() {
            conn.close;

            app.connections.remove(fd);

            app.generalLog.tracef(
                "[%d] Connection closed",
                fd
            );
        }

        //{{{ handleError
        void handleError(HTTPError err) {
            res.statusCode = err.status;

            if (app.errorhandler) {
                resgen = new Generator!string({
                    app.errorhandler(req, &res, err.status, err.msg);
                });
            } else {
                resgen = new Generator!string({
                    res.header("Content-Type", "text/html");
                    yieldchunk(format(`<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>%d Error</title>
    </head>
    <body>
        <h1>%d %s</h1>
        <h3>%s</h3>
    </body>
</html>`,
                        err.status,
                        err.status,
                        StatusCodes[err.status],
                        err.msg
                    ));
                });
            }
        }
        //}}}

        bool handleData() {
            char[1024] recvbuf;
            bool jump = false;

            long recv = conn.receive(recvbuf);
            
            if (recv == 0) {
                return false;
            }

            long i = 0;
            char cbuf;
            while (i < recv || jump || state == State.SendFixedLength
                                    || state == State.SendChunked
                                    || state == State.Finish) {
                // emulate computed goto
                if (jump) {
                    jump = false;
                } else if (state != State.SendFixedLength
                        && state != State.SendChunked
                        && state != State.Finish) {
                    cbuf = recvbuf[i++];

                    reqlen++;
                    if (parseHeaders && reqlen > MAX_HEADER_SIZE) {
                        error = new HTTPError(
                            400,
                            "Request headers too long"
                        );
                        state = State.SendHeaders;
                    } else if (!parseHeaders && reqlen > MAX_ENTITY_SIZE) {
                        error = new HTTPError(
                            413,
                            "Request entity too long"
                        );
                        state = State.SendHeaders;
                    }
                }

                //{{{ FSM
                final switch (state) {
                case State.Start:
                    parseHeaders = true;
                    // read status line
                    jump = doJump = true;
                    state = State.ReadUntilCRLF;
                    nextState0 = State.ParseHTTP1StatusLine;
                    break;

                case State.Finish:
                    SysTime time = Clock.currTime;
                    auto tz = time.utcOffset.split!("hours", "minutes");

                    string userAgent = req.getHeaderValue("User-Agent");

                    // write entry in access log
                    // TODO: remove logger overhead
                    app.accessLog.infof(
                        "%s - %s [%02d/%s/%02d:%02d:%02d:%02d %02d%02d]"
                      ~ " \"%s %s HTTP/%s\" %d %d \"%s\"",

                        req.address.toAddrString,
                        "-", // TODO

                        time.day,
                        time.month.to!string.capitalize,
                        time.year,
                        time.hour,
                        time.minute,
                        time.second,

                        tz.hours,
                        tz.minutes,

                        req.method,
                        spath,
                        VersionNames[req.httpVersion],

                        res.statusCode,
                        reslen,

                        userAgent ? userAgent : "-"
                    );

                    // if the connection is not keep alive close it
                    if (res.getHeaderValue("Connection") == "close") {
                        close;
                        return false;
                    }

                    // reset the data
                    reset;

                    // make sure the event is reset to listen only
                    DefaultEventLoop.set(fd, Event.Read, &app.handleClient);
                    DefaultEventLoop.set(fd, Event.Write, null);

                    break;

                case State.Proxy1:
                    Captures!string line = buf.matchFirst(Proxy);

                    if (!line) {
                        app.generalLog.tracef(
                            "[%d] Invalid PROXYv1 line",
                            fd
                        );
                        close;
                    }

                    switch (line[1]) {
                    case "TCP4":
                        req.address = new InternetAddress(
                            line[2],
                            line[4].to!ushort
                        );
                        req.destAddress = new InternetAddress(
                            line[3],
                            line[5].to!ushort
                        );
                        break;
                    case "TCP6":
                        req.address = new Internet6Address(
                            line[2],
                            line[4].to!ushort
                        );
                        req.destAddress = new Internet6Address(
                            line[3],
                            line[5].to!ushort
                        );
                        break;
                    default:
                        app.generalLog.tracef(
                            "[%d] Invalid PROXYv1 protocol",
                            fd
                        );
                        close;
                        break;
                    }

                    app.generalLog.tracef(
                        "[%d] Revised connection: %s:%s <-> %s",
                        fd,
                        req.destAddress.toAddrString,
                        req.destAddress.toPortString,
                        req.address.toAddrString
                    );

                    buf = "";
                    state = State.Start;
                    break;

                case State.Proxy2Start:
                    buf = "";
                    jump = doJump = true;
                    state = State.ReadFixedLength;
                    r0 = 16;
                    nextState0 = State.Proxy2Header;
                    break;

                case State.Proxy2Header:
                    if (buf[0..12]
                     != "\x0D\x0A\x0D\x0A\x00\x0D\x0A\x51\x55\x49\x54\x0A") {
                        app.generalLog.tracef(
                            "[%d] Invalid PROXYv2 header",
                            fd
                        );
                        close;
                    }

                    if (buf[12] == 0x20) {
                        app.generalLog.tracef(
                            "[%d] PROXYv2 connection is LOCAL",
                            fd
                        );
                        state = State.Start;
                        break;
                    } else if (buf[12] == 0x21) {
                        app.generalLog.tracef(
                            "[%d] PROXYv2 connection is PROXY",
                            fd
                        );
                    } else {
                        app.generalLog.tracef(
                            "[%d] Invalid PROXYv2 command or version",
                            fd
                        );
                        close;
                    }

                    r1 = buf[13];

                    long tmp = r1 & 0xF;
                    if (tmp > 0x2) {
                        app.generalLog.tracef(
                            "[%d] Unsupported PROXYv2 protocol",
                            fd
                        );
                        close;
                    }

                    tmp = r1 & 0xF0;
                    if (tmp == 0 || tmp > 0x20) {
                        app.generalLog.tracef(
                            "[%d] Unsupported PROXYv2 address family",
                            fd
                        );
                        close;
                    }

                    doJump = true;
                    state = State.ReadFixedLength;
                    nextState0 = State.Proxy2Address;
                    r0 = (cast(long)buf[14] << 8) | buf[15];
                    buf = "";

                    break;

                case State.Proxy2Address:
                    switch (r1 & 0xF0) {
                        case 0x10:
                            req.address = new InternetAddress(
                                (cast(uint)buf[0] << 24)
                              | (cast(uint)buf[1] << 16)
                              | (cast(uint)buf[2] << 8)
                              |  cast(uint)buf[3],
                                (cast(ushort)buf[8] << 8)
                              |  cast(ushort)buf[9]
                            );
                            req.destAddress = new InternetAddress(
                                (cast(uint)buf[4] << 24)
                              | (cast(uint)buf[5] << 16)
                              | (cast(uint)buf[6] << 8)
                              |  cast(uint)buf[7],
                                (cast(ushort)buf[10] << 8)
                              |  cast(ushort)buf[11]
                            );
                            break;
                        case 0x20:
                            req.address = new Internet6Address(
                                cast(ubyte[16])buf[0..16],
                                (cast(ushort)buf[32] << 8)
                              |  cast(ushort)buf[33]
                            );
                            req.destAddress = new Internet6Address(
                                cast(ubyte[16])buf[16..32],
                                (cast(ushort)buf[34] << 8)
                              |  cast(ushort)buf[35]
                            );
                            break;
                        default:
                            break;
                    }

                    app.generalLog.tracef(
                        "[%d] Revised connection: %s:%s <-> %s",
                        fd,
                        req.destAddress.toAddrString,
                        req.destAddress.toPortString,
                        req.address.toAddrString
                    );

                    state = State.Start;
                    buf = "";

                    break;

                case State.SendHeaders:
                    if (proxyLevel == 255) {
                        if (req.getHeaderValue("X-Forwarded-For") != null) {
                            req.address = parseAddress(
                                req.getHeaderValue("X-Forwarded-For")
                            );
                        }
                        if (req.getHeaderValue("X-Real-IP") != null) {
                            req.destAddress = parseAddress(
                                req.getHeaderValue("X-Real-IP")
                            );
                        }
                        app.generalLog.tracef(
                            "[%d] Revised connection: %s:%s <-> %s",
                            fd,
                            req.destAddress.toAddrString,
                            req.destAddress.toPortString,
                            req.address.toAddrString
                        );
                    }

                    res.headers.clear;
                    res.httpVersion = req.httpVersion;
                    res.statusCode = 200;
                    res.maxAge = MAX_AGE_DEFAULT;

                    req.requestBody = buf;

                    // if the client is HTTP/1.0 or asks to close the
                    // connection, send Connection: close
                    if (req.getHeaderValue("Connection") == "close"
                     || res.httpVersion == Version.HTTP10) {
                        res.header("Connection", "close");
                    }

                    bool foundurl = false;
                    bool processed = false;

                    Method methods;

                    SysTime time = Clock.currTime;
                    auto tz = time.utcOffset.split!("hours", "minutes");

                    res.header("Server", "RSHT/" ~ VERSION);
                    res.header("Date", date1123(time));

                    if (error) {
                        // if there's an error, send it
                        handleError(error);
                        processed = true;
                    } else {
                        // iterate on all url bindings
                        foreach (URLBinding url; app.urls) {
                            Path path;
                            try {
                                path.parse(
                                    "http://"
                                  ~ req.getHeaderValue("Host")
                                  ~ spath,
                                    url.host,
                                    url.path
                                );
                            } catch (Throwable) {}

                            req.path = path;

                            // if a matching host and path is found
                            if (path.host && path.path) {
                                if (!url.hook) {
                                    foundurl = true;
                                    methods |= url.method;
                                }

                                // if the requested method is the binding's
                                // method OR the requested method is HEAD
                                // but the binding supports GET
                                if (req.method & url.method
                                 || (url.method & Method.GET
                                  && req.method == Method.HEAD)) {
                                    // try to handle the request
                                    try {
                                        resgen = new Generator!string({
                                            url.handler(
                                                req,
                                                &res
                                            );
                                        });
                                    } catch (HTTPError ex) {
                                        handleError(ex);
                                    }

                                    if (!resgen.empty) {
                                        processed = true;

                                        break;
                                    }
                                }
                            }
                        }

                        if (!processed) {
                            if (foundurl) {
                                Array!string methodsarr;

                                if (methods & Method.GET)
                                    methodsarr.insertBack("GET");
                                if (methods & Method.POST)
                                    methodsarr.insertBack("POST");
                                if (methods & Method.PUT)
                                    methodsarr.insertBack("PUT");
                                if (methods & Method.DELETE)
                                    methodsarr.insertBack("DELETE");

                                // if a handler for the specified host/url
                                // was found but not the matching method,
                                // send a 405
                                res.header(
                                    "Allow",
                                    methodsarr.array
                                );
                                handleError(
                                    new HTTPError(405, "Invalid method")
                                );
                            } else {
                                // else, 404
                                handleError(
                                    new HTTPError(404, "Unknown resource")
                                );
                            }
                        }
                    }

                    reslen = resgen.moveFront.to!long;
                    resgen.popFront;

                    bool needLength = true;

                    state = State.SendFixedLength;

                    // if no body allowed,
                    // clear it and set needLength to false
                    if ((res.statusCode >= 100 && res.statusCode < 200)
                     || res.statusCode == 204
                     || res.statusCode == 304
                     || req.method == Method.HEAD) {
                        needLength = false;
                        reset;
                    }

                    if (res.getHeaderValue("Connection") != "close"
                     && needLength) {
                        if (reslen < 0) {
                            // chunked
                            res.header(
                                "Transfer-Encoding",
                                "chunked"
                            );
                            state = State.SendChunked;
                        } else {
                            // set Content-Length if necessary
                            res.header(
                                "Content-Length",
                                reslen.to!string
                            );
                            state = State.SendFixedLength;
                        }
                    }

                    if (reslen < 0) {
                        reslen = 0;
                    }

                    if (res.maxAge < 0) {
                        res.header("Cache-Control", "max-age=2147483648");
                    } else if (res.maxAge > 0) {
                        res.header("Cache-Control", format(
                            "max-age=%d",
                            res.maxAge
                        ));
                    }

                    string sres = res.toString;

                    // listen for write ready
                    DefaultEventLoop.set(fd, Event.Write, &app.handleClient);
                    DefaultEventLoop.set(fd, Event.Read, null);

                    // send back header
                    conn.send(sres);

                    break;

                case State.SendFixedLength:
                    if (resgen.empty) {
                        state = State.Finish;
                    } else {
                        conn.send(resgen.moveFront);
                        resgen.popFront;
                    }

                    break;

                case State.SendChunked:
                    if (resgen.empty) {
                        state = State.Finish;
                    } else {
                        string s = resgen.moveFront;
                        reslen += s.length;
                        conn.send(s.length.to!string ~ "\r\n");
                        conn.send(s ~ "\r\n");
                        resgen.popFront;
                    }

                    break;

                case State.ParseHTTP1StatusLine:
                    jump = true;
                    state = State.SendHeaders;

                    Captures!string line;

                    try {
                        line = buf.matchFirst(StatusLine);
                    } catch (UTFException) {
                        error = new HTTPError(
                            400,
                            "Invalid UTF-8 sequence"
                        );
                        break;
                    }

                    if (!line) {
                        error = new HTTPError(400, "Invalid status line");
                        break;
                    }

                    // parse the method and fail if it isn't supported
                    immutable(Method)* j = (line[1] in MethodNames);

                    if (!j) {
                        error = new HTTPError(501, "Unsupported method");
                        break;
                    }

                    req.method = *j;

                    spath = line[2];

                    // parse the version and fail if it isn't supported
                    long k = VersionNames.countUntil(
                        line[3] ~ "." ~ line[4]
                    );

                    if (k == -1) {
                        error = new HTTPError(505, "Unsupported version");
                        break;
                    }

                    req.httpVersion = cast(Version)k;

                    buf = "";
                    doJump = true;
                    jump = false;
                    state = State.ReadUntilCRLF;
                    nextState0 = State.ParseHTTP1Header;
                    nextState1 = State.ParseHTTP1Body;
                    break;

                case State.ParseHTTP1Header:
                    jump = true;
                    state = State.SendHeaders;

                    if (buf.empty) {
                        state = nextState1;
                        parseHeaders = false;
                        break;
                    }

                    Captures!string line;

                    try {
                        line = buf.matchFirst(Header);
                    } catch (UTFException) {
                        error = new HTTPError(
                            400,
                            "Invalid UTF-8 sequence"
                        );
                        break;
                    }

                    if (!line) {
                        error = new HTTPError(400, "Header line invalid");
                        break;
                    }

                    string[]* header = (line[1] in req.headers);
                    if (!header) {
                        // if header doesn't exist, create the array
                        req.headers[line[1]] = [];
                    }
                    // add all values of the header
                    req.headers[line[1]] ~= line[2].split(",").map!(
                        x => x.strip
                    ).array;

                    buf = "";
                    doJump = true;
                    jump = false;
                    state = State.ReadUntilCRLF;
                    // nextState0 is already ParseHTTP1Header
                    break;

                case State.ParseHTTP1Body:
                    jump = true;
                    state = State.SendHeaders;

                    buf = "";

                    if (req.getHeaderValue("Transfer-Encoding")) {
                        switch (req.getHeaderValue("Transfer-Encoding")) {
                        case "chunked":
                            doJump = true;
                            jump = false;
                            state = State.ReadUntilCRLF;
                            nextState0 = State.ParseChunkLength;
                            break;
                        default:
                            error = new HTTPError(
                                501,
                                "Unknown Transfer-Encoding"
                            );
                        }
                    } else if (req.getHeaderValue("Content-Length")) {
                        try {
                            r0 = req.getHeaderValue(
                                "Content-Length"
                            ).to!long;

                            if (r0 < 0) {
                                throw new Exception("");
                            }

                            doJump = true;
                            jump = false;
                            state = State.ReadFixedLength;
                            nextState0 = State.SendHeaders;
                        } catch (Throwable) {
                            error = new HTTPError(
                                400,
                                "Invalid Content-Length"
                            );
                        }
                    }
                    break;

                case State.ParseChunkLength:
                    jump = true;
                    state = State.SendHeaders;

                    try {
                        r0 = buf.to!long(16) + 2;

                        if (r0 < 0) {
                            throw new Exception("");
                        }

                        doJump = true;
                        jump = false;
                        buf = "";
                        // r0 = parsed + 2 so 2 == 0 in this case
                        if (r0 == 2) {
                            state = State.ReadUntilCRLF;
                            nextState0 = State.ParseHTTP1Header;
                            nextState1 = State.SendHeaders;
                        } else {
                            state = State.ReadFixedLength;
                            nextState0 = State.ParseChunkBody;
                        }
                    } catch (Throwable) {
                        error = new HTTPError(
                            400,
                            "Invalid chunk length"
                        );
                    }
                    break;

                case State.ParseChunkBody:
                    if (!buf.endsWith("\r\n")) {
                        error = new HTTPError(
                            400,
                            "Chunk must end with CRLF"
                        );
                        jump = true;
                        state = State.SendHeaders;
                        break;
                    }

                    req.requestBody ~= buf[0..$-2];
                    buf = "";

                    doJump = true;
                    state = State.ReadUntilCRLF;
                    nextState0 = State.ParseChunkLength;
                    break;

                case State.ReadUntilCRLF:
                    buf ~= cbuf;

                    /// if it ends with CRLF, jump to next state
                    if (buf.endsWith("\r\n")) {
                        buf = buf[0..$-2];
                        state = nextState0;
                        jump = doJump;
                        doJump = false;
                    }
                    break;

                case State.ReadFixedLength:
                    buf ~= cbuf;

                    // if all data is read, jump to next state
                    if (--r0 == 0) {
                        state = nextState0;
                        jump = doJump;
                        doJump = false;
                    }
                    break;
                }

                if (state == State.SendFixedLength
                 || state == State.SendChunked) {
                    break;
                }
                //}}}
            }

            return true;
        }
    }
    //}}}

    //{{{ Definitions
    Array!URLBinding urls;

    void delegate(
        RequestMessage,
        ResponseMessage*,
        long,
        string
    ) errorhandler;

    ubyte[int] isProxy;

    Socket[int] listeners;
    SocketData[int] connections;
    //}}}
}
