# Changelog

## 1.2.2 (2018-06-19)

- Fix random range violations

## 1.2.1 (2018-06-19)

- Fix a dirty hack, remove reliance on static variables

## 1.2 (2018-06-19)

- Change from epoll event loop to devnt
- Now works on all POSIX platforms
- Major internal changes related to event handling

## 1.1.6 (2018-05-30)

- Fix a bug in Path

## 1.1.5 (2018-05-30)

- Fix a bug in Path
- Add URI decoding

## 1.1.4 (2018-05-17)

- Response bodies are now returned even if the connection is set to close

## 1.1.3 (2018-05-17)

- Fix crash when `X-Forwarded-For` is not present
- Add `X-Real-IP` support

## 1.1.2 (2018-05-17)

- Minor API additions
- Support for `X-Forwarded-For`

## 1.1.1 (2018-05-04)

- Fix bug where a route not supporting a certain method would return a 404
  instead of a 405 in certain cases

## 1.1 (2018-05-02)

- First release of RSHT!
- Linux only
- Mostly compliant implementation of HTTP/1.0 and HTTP/1.1
- Notably lacking is:
  - HTTP authentication
  - Content encodings
- Implementation of PROXYv1 and PROXYv2 protocol
