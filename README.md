# RSHT

![RSHT](rsht.png)

(Thanks to [szymszl](https://szymszl.xyz) for that logo)

RSHT (Named after רשת, the Hebrew word for network, pronounced [reʃet]) is a
simple web server framework written in D.

Note: Although RSHT should work on all POSIX platforms and Windows, it has
only been tested on Linux.

## Known issues

- RSHT can compile with DMD but makes regular expressions segfault.
  Use LDC2 or GDC instead.

## TLS

TLS is currently not supported in RSHT.

If you need TLS, it is recommended to use a proxy, such as
[stunnel](https://www.stunnel.org/).

## Versioning

Versions of RSHT are separated between development versions and stable
versions.

Development versions are in the form M.0, where M is the major version.

Stable versions are in the form M.m.P, where M is the major version, m is the
minor version (>=0) and P is the patch number (which is ommited if it is 0).

## TODO

- Low priority
  - [ ] HTTP/2 support
  - [ ] WebSocket support
  - [ ] Better access log format
  - [ ] HTTP authentication
  - [ ] Native TLS
  - [x] Support other event loops than epoll
- Medium priority
  - [ ] Content encodings
  - [x] Support more than one listener
  - [x] Caching
  - [ ] Multiprocessing
- High priority
  - [x] Correct message length handling
  - [x] Support HEAD
  - [x] Chunked encoding
  - [x] Redirects
  - [x] Correct handling of large files
